/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 13:50:17 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/10 14:10:59 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int n)
{
	long	nb;
	long	i;
	long	pwr;
	char	str[12];

	nb = (long)n;
	i = 0;
	pwr = 1;
	if (nb < 0)
	{
		nb = -nb;
		str[i] = '-';
		i++;
	}
	if (nb == 0)
		pwr *= 10;
	while (nb / pwr > 0)
		pwr *= 10;
	while (pwr /= 10)
		str[i++] = (nb / pwr) % 10 + '0';
	str[i] = 0;
	write(1, str, i);
}
