/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 20:20:40 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/10 13:06:00 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t		i;
	const char	*tp_s1;
	const char	*tp_s2;

	i = 0;
	tp_s1 = s1;
	tp_s2 = s2;
	while (i < n)
	{
		if (tp_s1[i] != tp_s2[i])
			return ((unsigned char)tp_s1[i] - (unsigned char)tp_s2[i]);
		i++;
	}
	return (0);
}
