/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 16:55:18 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 15:02:41 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static size_t	ft_firstchar_index(char *str)
{
	size_t	i;

	i = 0;
	while (str[i] && (str[i] == ' ' || str[i] == '\n' || str[i] == '\t'))
		i++;
	return (i);
}

static size_t	ft_lastchar_index(char *str)
{
	size_t	i;

	i = 0;
	while (str[i])
		i++;
	i--;
	while ((str[i] == ' ' || str[i] == '\n' || str[i] == '\t') && i > 0)
		i--;
	return (i);
}

char			*ft_strtrim(char const *s)
{
	size_t	first_char;
	size_t	last_char;
	size_t	i;
	char	*trim;

	if (!s)
		return (NULL);
	if (*s == 0)
		return ("");
	first_char = ft_firstchar_index((char*)s);
	last_char = ft_lastchar_index((char*)s);
	i = last_char - first_char + 1;
	if (first_char > last_char)
		i = 0;
	if (!(trim = (char*)malloc(sizeof(*trim) * (i + 1))))
		return (NULL);
	i = 0;
	while (first_char + i <= last_char)
	{
		trim[i] = s[first_char + i];
		i++;
	}
	trim[i] = 0;
	return (trim);
}
