/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 18:36:55 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/13 21:02:35 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static char	*malloc_str(int n)
{
	int		pwr;
	int		i;
	long	nb;

	i = 1;
	pwr = 1;
	nb = (long)n;
	if (nb < 0)
	{
		nb = -nb;
		i++;
	}
	if (nb == 0)
		i++;
	while (nb / pwr > 0)
	{
		pwr *= 10;
		i++;
	}
	return ((char*)malloc(sizeof(char) * i));
}

char		*ft_itoa(int n)
{
	long	nb;
	long	i;
	long	pwr;
	char	*str;

	nb = (long)n;
	i = 0;
	pwr = 1;
	if (!(str = malloc_str(n)))
		return (NULL);
	if (nb < 0)
	{
		nb = -nb;
		str[i] = '-';
		i++;
	}
	if (nb == 0)
		pwr *= 10;
	while (nb / pwr > 0)
		pwr *= 10;
	while (pwr /= 10)
		str[i++] = (nb / pwr) % 10 + '0';
	str[i] = 0;
	return (str);
}
