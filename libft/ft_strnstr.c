/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 17:02:33 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/10 13:42:25 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	size_t	j;
	char	*tp_haystack;
	char	*tp_needle;

	i = 0;
	tp_haystack = (char*)haystack;
	tp_needle = (char*)needle;
	if (*needle == 0)
		return (tp_haystack);
	while (tp_haystack[i] && i < len)
	{
		j = 0;
		if (tp_haystack[i] == tp_needle[j])
		{
			while (tp_haystack[i + j] == tp_needle[j] && tp_needle[j]
					&& tp_haystack[i + j]
					&& i + j < len)
				j++;
			if (tp_needle[j] == '\0')
				return ((char*)&tp_haystack[i]);
		}
		i++;
	}
	return (NULL);
}
