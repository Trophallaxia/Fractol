/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 13:45:02 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 16:56:58 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strrev(char *str)
{
	char	c;
	size_t	i;
	size_t	n;

	c = 'c';
	i = 0;
	n = ft_strlen(str) - 1;
	while (i <= n / 2)
	{
		c = str[i];
		str[i] = str[n - i];
		str[n - i] = c;
		i++;
	}
	return (str);
}
