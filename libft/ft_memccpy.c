/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 12:09:04 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 13:32:36 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char		*tp_dst;
	unsigned char		*tp_src;

	tp_dst = (unsigned char*)dst;
	tp_src = (unsigned char*)src;
	if (!n)
		return (NULL);
	while (*tp_src != (unsigned char)c && n--)
		*tp_dst++ = *tp_src++;
	if (n == 0)
		return (NULL);
	else if (*tp_src == (unsigned char)c && n)
	{
		*tp_dst++ = *tp_src;
		return ((void*)tp_dst);
	}
	return (NULL);
}
