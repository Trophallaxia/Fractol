/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_2D_arinit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 12:09:18 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 18:06:52 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	**ft_2d_arinit(size_t col, size_t row)
{
	int		**array;
	size_t	i;

	i = 0;
	array = (int**)malloc(sizeof(*array) * row);
	while (i < row)
	{
		array[i] = ft_arinit(col);
		i++;
	}
	return (array);
}
