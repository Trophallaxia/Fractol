/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 16:17:55 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/07 21:46:42 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(const char *haystack, const char *needle)
{
	char	*tp_hay;
	char	*tp_nee;
	char	*start;

	tp_hay = (char*)haystack;
	if (needle[0] == '\0')
		return ((char*)haystack);
	while (*tp_hay)
	{
		tp_nee = (char*)needle;
		if (*tp_hay == needle[0])
		{
			start = tp_hay;
			while (*tp_hay == *tp_nee && *tp_nee && *tp_hay)
			{
				tp_nee++;
				tp_hay++;
			}
			if (*tp_nee == '\0')
				return (start);
			tp_hay = start;
		}
		tp_hay++;
	}
	return ((void *)0);
}
