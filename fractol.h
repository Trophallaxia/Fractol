/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 12:08:55 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/09 21:38:15 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include "libft.h"
# include <mlx.h>
# include <stdlib.h>
# include <unistd.h>
# include <pthread.h>
# include <math.h>
# include <time.h>

# define WIDTH 1920
# define HEIGHT 1080
# define WIDTH_MULT 2560
# define WIDTH_MINI 640
# define HEIGHT_MINI 360
# define MAX_FRACT 7

typedef struct	s_img
{
	void		*ptr;
	char		*str;
	int			bpp;
	int			s_l;
	int			endian;
}				t_img;

typedef	struct	s_choice
{
	char		*params[MAX_FRACT];
	double		mdbrt[4];
	double		jul[4];
	double		shp[4];
	double		hrt[4];
	double		tri[4];
	double		cel[4];
	double		chm[4];
	double		*choice[MAX_FRACT];
}				t_choice;

typedef struct	s_fract
{
	double		x1;
	double		x2;
	double		y1;
	double		y2;
	double		zoom_x;
	double		zoom_y;
}				t_fract;

typedef struct	s_rgb
{
	int			r;
	int			g;
	int			b;
	int			n;
}				t_rgb;

typedef struct	s_env
{
	int			op;
	void		*mlx;
	void		*win;
	int			wdth;
	double		zoom;
	double		it_max;
	int			y;
	int			y_max;
	double		cr;
	double		ci;
	int			j;
	int			h;
	int			p;
	int			(*fract[MAX_FRACT])(double x, double y,\
			struct s_env **p, t_fract *f);
	t_img		img;
	t_fract		f;
	t_rgb		rgb;
	t_choice	c;
}				t_env;

typedef struct	s_point
{
	double		r;
	double		i;
}				t_point;

/*
**Check functions
*/

void			usage(void);
int				check_params(char **av, t_env *p);

/*
** Init functions
*/

void			init(int op, t_env **env, double *tab);
t_fract			init_f(double *tab, int i);
t_rgb			init_rgb(t_rgb c, int i);
void			init_choice(t_env **p);

/*
** Events functions
*/
int				key_hook(int key, t_env *p);
int				mouse_hook(int butt, int x, int y, t_env *p);
int				rejuv(int x, int y, t_env *p);
void			reset(t_env **p);
void			display_ui(t_env *p);

/*
** Draw functions
*/
void			multi_thread(t_env *fractol);
void			redraw(t_env **p);
void			solo_fract(t_env *p);
void			fractol(t_env **p);
void			color(int x, int y, int i, t_env **p);
void			rot_color(int key, t_env **p);
void			reset(t_env **p);

/*
** Fractales functions
*/

void			minibrot1(t_env **p);
void			minilia(t_env **p);
void			miniship(t_env **p);
int				mandelbrot(double x, double y, t_env **p, t_fract *f);
int				julia(double x, double y, t_env **p, t_fract *f);
int				ship(double x, double y, t_env **p, t_fract *f);
int				heart(double x, double y, t_env **p, t_fract *f);
int				tricorn(double x, double y, t_env **p, t_fract *f);
int				celtic(double x, double y, t_env **p, t_fract *f);
int				chameleon(double x, double y, t_env **p, t_fract *f);

#endif
