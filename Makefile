# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/19 20:00:00 by sabonifa          #+#    #+#              #
#    Updated: 2019/09/09 18:23:03 by sabonifa         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol 

HEADER = fractol.h

SRC = src/fractol.c \
	  src/init.c \
	  src/julia.c \
	  src/key_events.c \
	  src/mandelbrot.c \
	  src/mouse_hook.c \
	  src/color.c \
	  src/more.c\
	  src/burning_ship.c \
	  src/main.c

OBJ = $(SRC:.c=.o)

FLAGS = -Wall -Wextra -Werror -O2
LIB = libft/libft.a
LFT_INC = libft/includes
LFT = libft -lft
LX_INC = /usr/local/include
LX = /usr/local/lib -lmlx -framework OpenGL -framework AppKit

CC = gcc

all : $(NAME)

%.o : %.c $(SRC)
	@$(CC) $(FLAGS) -I . -I libft/includes -o $@ -c $<

force :
	@true

$(LIB) : force
	@echo "Make libft"
	@make -C libft/

$(NAME) : $(LIB) $(OBJ) fractol.h  Makefile
	@echo "Making..."
	@$(CC) $(FLAGS) -I . -I $(LFT_INC) -L $(LFT) -I $(LX_INC) -L $(LX)  -o $@ $(OBJ)
	@echo "Made !"

clean :
	@echo "Cleaning objects"
	@rm -rf $(OBJ)
	@make -C libft/ clean

fclean : clean
	@echo "Cleaning all"
	@rm -rf $(NAME)
	@make -C libft/ fclean

re : fclean all
.PHONY: all, clean, fclean, re, force
