/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_hook.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/27 20:15:25 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/09 20:07:20 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		rejuv(int x, int y, t_env *p)
{
	if (x > 0 && x < WIDTH && y > 0 && y < HEIGHT && p->j == 0 &&\
			(p->op % 10 == 1 || p->op % 10 == 6))
	{
		(p)->cr = (double)x / p->f.zoom_x + p->f.x1;
		(p)->ci = (double)y / p->f.zoom_y + p->f.y1;
		redraw(&p);
	}
	return (0);
}

double	interpolate(double start, double end, double interpolation)
{
	return (start + ((end - start) * interpolation));
}

void	mouse_zoom(int butt, double x, double y, t_env **p)
{
	x = x / (*p)->f.zoom_x + (*p)->f.x1;
	y = y / (*p)->f.zoom_y + (*p)->f.y1;
	if (butt == 5)
		(*p)->zoom = (*p)->zoom > 1 ? 1 : (*p)->zoom - 0.01;
	if (butt == 4)
		(*p)->zoom = (*p)->zoom < 1 ? 1 : (*p)->zoom + 0.01;
	if (butt == 4 || butt == 5)
	{
		(*p)->f.x1 = interpolate(x, (*p)->f.x1, (*p)->zoom);
		(*p)->f.y1 = interpolate(y, (*p)->f.y1, (*p)->zoom);
		(*p)->f.x2 = interpolate(x, (*p)->f.x2, (*p)->zoom);
		(*p)->f.y2 = interpolate(y, (*p)->f.y2, (*p)->zoom);
	}
	(*p)->f.zoom_x = WIDTH / ((*p)->f.x2 - (*p)->f.x1);
	(*p)->f.zoom_y = HEIGHT / ((*p)->f.y2 - (*p)->f.y1);
	redraw(p);
}

void	mouse_switch(int y, t_env *p)
{
	if (y > 0 && y < HEIGHT_MINI)
		p->op = 20;
	if (y > HEIGHT_MINI && y < HEIGHT_MINI * 2)
		p->op = 21;
	if (y > HEIGHT_MINI * 2 && y < HEIGHT_MINI * 3)
		p->op = 22;
	reset(&p);
}

int		mouse_hook(int butt, int x, int y, t_env *p)
{
	if (butt == 4 || butt == 5)
		mouse_zoom(butt, x, y, &p);
	if (butt == 1 && x > WIDTH && x < WIDTH_MULT)
		mouse_switch(y, p);
	if (butt == 2)
		p->j = p->j % 2 == 0 ? 1 : 0;
	return (0);
}
