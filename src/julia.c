/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/06 13:07:12 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/09 20:38:23 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		julia(double x, double y, t_env **p, t_fract *f)
{
	double	tmp;
	t_point	z;
	t_point	c;
	int		i;

	i = 0;
	c.r = (*p)->cr;
	c.i = (*p)->ci;
	z.r = x / f->zoom_x + f->x1;
	z.i = y / f->zoom_y + f->y1;
	while (z.r * z.r + z.i * z.i < 4 && i < (*p)->it_max)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = 2 * z.i * tmp + c.i;
		i++;
	}
	return (i);
}

void	minilia(t_env **p)
{
	t_fract f;
	int		x;
	int		y;

	f = init_f((*p)->c.jul, 2);
	x = -1;
	while (++x < WIDTH_MINI)
	{
		y = -1;
		while (++y < HEIGHT_MINI)
		{
			color(x + WIDTH, y + HEIGHT_MINI, (*p)->fract[1](x, y, p, &f), p);
		}
	}
}
