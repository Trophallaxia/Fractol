/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heart.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 18:17:53 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/09 21:40:40 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		heart(double x, double y, t_env **p, t_fract *f)
{
	double	tmp;
	t_point z;
	t_point c;
	int		i;

	i = 0;
	z.r = 0;
	z.i = 0;
	c.r = x / f->zoom_x + f->x1;
	c.i = y / f->zoom_y + f->y1;
	while (z.r * z.r + z.i * z.i < 4 && i < (*p)->it_max)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = 2 * z.i * fabs(tmp) + c.i;
		i++;
	}
	return (i);
}

int		tricorn(double x, double y, t_env **p, t_fract *f)
{
	double	tmp;
	t_point	z;
	t_point	c;
	int		i;

	i = 0;
	z.r = 0;
	z.i = 0;
	c.r = x / f->zoom_x + f->x1;
	c.i = y / f->zoom_y + f->y1;
	while (z.r * z.r + z.i * z.i < 4 && i < (*p)->it_max)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = -2 * z.i * (tmp) + c.i;
		i++;
	}
	return (i);
}

int		celtic(double x, double y, t_env **p, t_fract *f)
{
	double	tmp;
	t_point	z;
	t_point	c;
	int		i;

	i = 0;
	z.r = 0;
	z.i = 0;
	c.r = x / f->zoom_x + f->x1;
	c.i = y / f->zoom_y + f->y1;
	while (z.r * z.r + z.i * z.i < 4 && i < (*p)->it_max)
	{
		tmp = z.r;
		z.r = fabs(z.r * z.r - z.i * z.i) + c.i;
		z.i = 2 * z.i * (tmp) + c.r;
		i++;
	}
	return (i);
}

int		chameleon(double x, double y, t_env **p, t_fract *f)
{
	double	tmp;
	t_point	z;
	t_point	c;
	int		i;

	i = 0;
	c.r = (*p)->cr;
	c.i = (*p)->ci;
	z.r = x / f->zoom_x + f->x1;
	z.i = y / f->zoom_y + f->y1;
	while (z.r * z.r + z.i * z.i < 4 && i < (*p)->it_max)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = -2 * z.i * tmp + c.i;
		i++;
	}
	return (i);
}
