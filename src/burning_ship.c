/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   burning_ship.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/06 15:18:32 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/09 17:29:10 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ship(double x, double y, t_env **p, t_fract *f)
{
	double	tmp;
	t_point	z;
	t_point	c;
	int		i;

	i = 0;
	z.r = 0;
	z.i = 0;
	c.r = x / f->zoom_x + f->x1;
	c.i = y / f->zoom_y + f->y1;
	while (z.r * z.r + z.i * z.i < 4 && i < (*p)->it_max)
	{
		z.r = z.r > 0 ? z.r : -z.r;
		z.i = z.i > 0 ? z.i : -z.i;
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = 2 * z.i * tmp + c.i;
		i++;
	}
	return (i);
}

void	miniship(t_env **p)
{
	t_fract f;
	int		x;
	int		y;

	f = init_f((*p)->c.shp, 2);
	x = -1;
	while (++x < WIDTH_MINI)
	{
		y = -1;
		while (++y < HEIGHT_MINI)
		{
			color(x + WIDTH, y + HEIGHT_MINI * 2,\
					(*p)->fract[2](x, y, p, &f), p);
		}
	}
}
