/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/06 12:56:42 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/09 18:49:13 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		mandelbrot(double x, double y, t_env **p, t_fract *f)
{
	double	tmp;
	t_point	z;
	t_point	c;
	int		i;

	i = 0;
	z.r = 0;
	z.i = 0;
	c.r = x / f->zoom_x + f->x1;
	c.i = y / f->zoom_y + f->y1;
	while (z.r * z.r + z.i * z.i < 4 && i < (*p)->it_max)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = 2 * z.i * tmp + c.i;
		i++;
	}
	return (i);
}

void	minibrot1(t_env **p)
{
	t_fract	f;
	int		x;
	int		y;

	f = init_f((*p)->c.mdbrt, 2);
	x = -1;
	while (++x < WIDTH_MINI)
	{
		y = -1;
		while (++y < HEIGHT_MINI)
		{
			color(x + WIDTH, y, (*p)->fract[0](x, y, p, &f), p);
		}
	}
}
