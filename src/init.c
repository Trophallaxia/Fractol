/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 14:40:23 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/10 10:35:27 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_choice_bis(t_env **p)
{
	(*p)->c.tri[0] = -2;
	(*p)->c.tri[1] = 1.5;
	(*p)->c.tri[2] = -2;
	(*p)->c.tri[3] = 2;
	(*p)->c.cel[0] = -2;
	(*p)->c.cel[1] = 2;
	(*p)->c.cel[2] = -2;
	(*p)->c.cel[3] = 0.4;
	(*p)->c.chm[0] = -2;
	(*p)->c.chm[1] = 2;
	(*p)->c.chm[2] = -2;
	(*p)->c.chm[3] = 2;
	(*p)->c.choice[0] = (*p)->c.mdbrt;
	(*p)->c.choice[1] = (*p)->c.jul;
	(*p)->c.choice[2] = (*p)->c.shp;
	(*p)->c.choice[3] = (*p)->c.hrt;
	(*p)->c.choice[4] = (*p)->c.tri;
	(*p)->c.choice[5] = (*p)->c.cel;
	(*p)->c.choice[6] = (*p)->c.chm;
	(*p)->fract[1] = julia;
	(*p)->fract[2] = ship;
	(*p)->fract[3] = heart;
	(*p)->fract[4] = tricorn;
	(*p)->fract[5] = celtic;
	(*p)->fract[6] = chameleon;
}

void	init_choice(t_env **p)
{
	(*p)->c.params[0] = "Mandelbrot";
	(*p)->c.params[1] = "Julia";
	(*p)->c.params[2] = "Burning_Ship";
	(*p)->c.params[3] = "Heart";
	(*p)->c.params[4] = "Tricorn";
	(*p)->c.params[5] = "Celtic";
	(*p)->c.params[6] = "Chameleon";
	(*p)->c.mdbrt[0] = -2.1;
	(*p)->c.mdbrt[1] = 0.6;
	(*p)->c.mdbrt[2] = -1.2;
	(*p)->c.mdbrt[3] = 1.2;
	(*p)->c.jul[0] = -2;
	(*p)->c.jul[1] = 2;
	(*p)->c.jul[2] = -2;
	(*p)->c.jul[3] = 2;
	(*p)->c.shp[0] = -2;
	(*p)->c.shp[1] = 1.5;
	(*p)->c.shp[2] = -2;
	(*p)->c.shp[3] = 1;
	(*p)->c.hrt[0] = -2.1;
	(*p)->c.hrt[1] = 0.6;
	(*p)->c.hrt[2] = -1.2;
	(*p)->c.hrt[3] = 1.2;
	(*p)->fract[0] = mandelbrot;
	init_choice_bis(p);
}

t_rgb	init_rgb(t_rgb c, int i)
{
	if (i == 0)
	{
		c.r = 0xFF;
		c.g = 0xFF;
		c.b = 0xFF;
	}
	else
	{
		srand(time(NULL));
		c.r = rand() % 0xFF;
		c.g = rand() % 0xFF;
		c.b = rand() % 0xFF;
		c.n = (double)rand() / RAND_MAX * 256 * 256 * 256;
	}
	return (c);
}

void	init(int op, t_env **env, double *tab)
{
	(*env)->op = op;
	(*env)->j = 0;
	(*env)->h = 1;
	(*env)->p = 0;
	(*env)->cr = -0.272917;
	(*env)->ci = -0.670370;
	(*env)->mlx = mlx_init();
	(*env)->zoom = 1.0;
	(*env)->it_max = 100;
	(*env)->f = init_f(tab, 0);
	(*env)->rgb = init_rgb((*env)->rgb, 0);
	if (op / 10 == 1)
		(*env)->wdth = WIDTH;
	if (op / 10 == 2)
		(*env)->wdth = WIDTH_MULT;
	(*env)->win = mlx_new_window((*env)->mlx, (*env)->wdth, HEIGHT,\
			"Fract'ol");
	(*env)->img.ptr = mlx_new_image((*env)->mlx, (*env)->wdth, HEIGHT);
	(*env)->img.str = mlx_get_data_addr((*env)->img.ptr,\
			&(*env)->img.bpp, &(*env)->img.s_l, &(*env)->img.endian);
}

t_fract	init_f(double *tab, int i)
{
	t_fract	f;

	f.x1 = tab[0];
	f.x2 = tab[1];
	f.y1 = tab[2];
	f.y2 = tab[3];
	if (i == 2)
	{
		f.zoom_x = WIDTH_MINI / (f.x2 - f.x1);
		f.zoom_y = HEIGHT_MINI / (f.y2 - f.y1);
	}
	else
	{
		f.zoom_x = WIDTH / (f.x2 - f.x1);
		f.zoom_y = HEIGHT / (f.y2 - f.y1);
	}
	return (f);
}
