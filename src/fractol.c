/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 13:06:13 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/09 17:19:51 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	redraw(t_env **p)
{
	if ((*p)->it_max < (190 - (*p)->zoom * 100))
		(*p)->it_max += 10;
	if ((*p)->it_max > (270 - (*p)->zoom * 100))
		(*p)->it_max -= 10;
	fractol(p);
}

void	multi_thread(t_env *env)
{
	int			i;
	t_env		tab[108];
	pthread_t	t[108];

	i = 0;
	while (i < 108)
	{
		ft_memcpy((void*)&tab[i], (void*)env, sizeof(t_env));
		tab[i].y = 10 * i;
		tab[i].y_max = 10 * (i + 1);
		pthread_create(&t[i], NULL, (void*)solo_fract, &tab[i]);
		i++;
	}
	while (i--)
		pthread_join(t[i], NULL);
}

void	solo_fract(t_env *p)
{
	int x;
	int	y;
	int tmp;

	x = -1;
	tmp = (p)->y;
	while (++x < WIDTH)
	{
		y = tmp - 1;
		while (++y < (p)->y_max)
		{
			color(x, y, p->fract[p->op % 10](x, y, &p, &(p)->f), &p);
		}
	}
}

void	fractol(t_env **p)
{
	multi_thread(*p);
	if ((*p)->op > 19)
	{
		minibrot1(p);
		minilia(p);
		miniship(p);
	}
	mlx_put_image_to_window((*p)->mlx, (*p)->win, (*p)->img.ptr, 0, 0);
	display_ui(*p);
}
