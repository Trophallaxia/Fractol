/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 19:24:25 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/09 20:07:30 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	rot_color(int key, t_env **p)
{
	if (key == 31)
		(*p)->rgb = init_rgb((*p)->rgb, 1);
	if (key == 35)
		(*p)->p = (*p)->p == 0 ? 1 : 0;
	if (key == 17)
	{
		(*p)->rgb.r = 255;
		(*p)->rgb.g = 255;
		(*p)->rgb.b = 255;
	}
	redraw(p);
}

void	color_psy(int x, int y, int i, t_env **p)
{
	double	t;
	int		c;

	t = ((*p)->it_max - i) / (*p)->it_max;
	c = (*p)->rgb.n * t;
	(*p)->img.str[x * 4 + y * (*p)->wdth * 4] = 0xFF & c;
	(*p)->img.str[x * 4 + y * (*p)->wdth * 4 + 1] = (0xFF00 & c) >> 8;
	(*p)->img.str[x * 4 + y * (*p)->wdth * 4 + 2] = (0xFF0000 & c) >> 16;
}

void	color(int x, int y, int i, t_env **p)
{
	double	t;
	int		c;

	if (i == (*p)->it_max)
	{
		(*p)->img.str[x * 4 + y * (*p)->wdth * 4] = 0;
		(*p)->img.str[x * 4 + y * (*p)->wdth * 4 + 1] = 0;
		(*p)->img.str[x * 4 + y * (*p)->wdth * 4 + 2] = 0;
	}
	else
	{
		t = (i) / (*p)->it_max;
		c = ((int)(9 * (1 - t) * t * t * t * (*p)->rgb.r) << 16)\
			+ ((int)(15 * (1 - t) * (1 - t) * t * t * (*p)->rgb.g) << 8)\
			+ ((int)(8.5 * (1 - t) * (1 - t) * (1 - t) * t * (*p)->rgb.b));
		(*p)->img.str[x * 4 + y * (*p)->wdth * 4] = (0xFF & c);
		(*p)->img.str[x * 4 + y * (*p)->wdth * 4 + 1] = (0xFF00 & c) >> 8;
		(*p)->img.str[x * 4 + y * (*p)->wdth * 4 + 2] = (0xFF0000 & c) >> 16;
		if ((*p)->p == 1)
			color_psy(x, y, i, p);
	}
}

void	reset(t_env **p)
{
	(*p)->f.x1 = (*p)->c.choice[(*p)->op % 10][0];
	(*p)->f.x2 = (*p)->c.choice[(*p)->op % 10][1];
	(*p)->f.y1 = (*p)->c.choice[(*p)->op % 10][2];
	(*p)->f.y2 = (*p)->c.choice[(*p)->op % 10][3];
	(*p)->f.zoom_x = WIDTH / ((*p)->f.x2 - (*p)->f.x1);
	(*p)->f.zoom_y = HEIGHT / ((*p)->f.y2 - (*p)->f.y1);
	(*p)->cr = -0.272917;
	(*p)->ci = -0.670370;
	(*p)->zoom = 1.0;
	(*p)->it_max = 100;
	redraw(p);
}
