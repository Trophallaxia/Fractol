/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/27 17:14:27 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/10 11:05:17 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	display_help(t_env *p)
{
	mlx_string_put(p->mlx, p->win, 10, 860, 0xFFFFFF, "Help :");
	mlx_string_put(p->mlx, p->win, 200, 860, 0xFFFFFF, "H");
	mlx_string_put(p->mlx, p->win, 10, 890, 0xFFFFFF, "Psych :");
	mlx_string_put(p->mlx, p->win, 200, 890, 0xFFFFFF, "P");
	mlx_string_put(p->mlx, p->win, 10, 920, 0xFFFFFF, "Rotate :");
	mlx_string_put(p->mlx, p->win, 200, 920, 0xFFFFFF, "O , I");
	mlx_string_put(p->mlx, p->win, 10, 950, 0xFFFFFF, "Reset colors");
	mlx_string_put(p->mlx, p->win, 200, 950, 0xFFFFFF, "T");
	mlx_string_put(p->mlx, p->win, 10, 980, 0xFFFFFF, "Reset :");
	mlx_string_put(p->mlx, p->win, 200, 980, 0xFFFFFF, "R");
	mlx_string_put(p->mlx, p->win, 10, 1010, 0xFFFFFF, "Arrow keys :");
	mlx_string_put(p->mlx, p->win, 200, 1010, 0xFFFFFF, "Move map");
	mlx_string_put(p->mlx, p->win, 10, 1040, 0xFFFFFF, "Dyn Julia/Cham :");
	mlx_string_put(p->mlx, p->win, 200, 1040, 0xFFFFFF, "Right click");
}

void	display_ui(t_env *p)
{
	int		c;
	int		z;
	char	*s;

	c = p->op % 10;
	z = (p->c.choice[c][1] - p->c.choice[c][0]) * 100 / (p->f.x2 - p->f.x1);
	mlx_string_put(p->mlx, p->win, 10, 10, 0xFFFFFF, "Iteration :");
	s = ft_itoa(p->it_max);
	mlx_string_put(p->mlx, p->win, 200, 10, 0xFFFFFF, s);
	free(s);
	s = ft_itoa(z);
	mlx_string_put(p->mlx, p->win, 10, 30, 0xFFFFFF, "Zoom % :");
	mlx_string_put(p->mlx, p->win, 200, 30, 0xFFFFFF,
			z < -2147483647 ? "OVER 9000 !" : s);
	free(s);
	mlx_string_put(p->mlx, p->win, 10, 50, 0xFFFFFF, p->c.params[c]);
	if (p->op > 19)
	{
		mlx_string_put(p->mlx, p->win, WIDTH, 320, 0xFFFFFF, "Mandelbrot");
		mlx_string_put(p->mlx, p->win, WIDTH, 690, 0xFFFFFF, "Julia");
		mlx_string_put(p->mlx, p->win, WIDTH, 1050, 0xFFFFFF, "Burning Ship");
	}
	if (p->h == 1)
	{
		display_help(p);
	}
}

void	move_map(int key, t_env **p)
{
	double	k;

	k = fabs((*p)->f.y1 - (*p)->f.y2);
	if (key == 124)
	{
		(*p)->f.x1 -= 0.1 * k;
		(*p)->f.x2 -= 0.1 * k;
	}
	if (key == 123)
	{
		(*p)->f.x1 += 0.1 * k;
		(*p)->f.x2 += 0.1 * k;
	}
	if (key == 125)
	{
		(*p)->f.y1 -= 0.1 * k;
		(*p)->f.y2 -= 0.1 * k;
	}
	if (key == 126)
	{
		(*p)->f.y1 += 0.1 * k;
		(*p)->f.y2 += 0.1 * k;
	}
	redraw(p);
}

void	change_it(int key, t_env **p)
{
	if (key == 78)
		(*p)->it_max = (*p)->it_max - 10 >= 10 ? (*p)->it_max - 10 : 10;
	if (key == 69)
		(*p)->it_max += 10;
	redraw(p);
}

int		key_hook(int key, t_env *p)
{
	int	tmp;

	tmp = (p)->op / 10;
	if (key == 53)
		exit(0);
	if (key >= 123 && key <= 126)
		move_map(key, &p);
	if (key == 69 || key == 78)
		change_it(key, &p);
	if (key == 15)
		reset(&p);
	if (key == 4)
	{
		p->h = p->h == 0 ? 1 : 0;
		redraw(&p);
	}
	if (key == 31 || key == 35 || key == 17)
		rot_color(key, &p);
	if (key == 34)
	{
		(p)->op += 1;
		(p)->op = 10 * (int)(tmp) + ((p)->op % 10) % MAX_FRACT;
		redraw(&p);
	}
	return (0);
}
