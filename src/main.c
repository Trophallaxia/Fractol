/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 12:37:44 by sabonifa          #+#    #+#             */
/*   Updated: 2019/09/10 11:09:09 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	usage(void)
{
	ft_putstr("\nusage : ./fractol <param> <option>\n\n");
	ft_putstr("- Params :\n");
	ft_putstr("-Mandelbrot\n");
	ft_putstr("-Julia\n");
	ft_putstr("-Burning_Ship\n");
	ft_putstr("-Heart\n");
	ft_putstr("-Tricorn\n");
	ft_putstr("-Celtic\n");
	ft_putstr("-Chameleon\n\n");
	ft_putstr("- Options :\n");
	ft_putstr("-Solo\n");
	ft_putstr("-Multi\n");
	exit(0);
}

int		check_params(char **av, t_env *p)
{
	int	i;
	int res;

	i = 0;
	res = -1;
	while (i < MAX_FRACT)
	{
		if (ft_strcmp(av[1], p->c.params[i]) == 0)
			res = i;
		i++;
	}
	if (ft_strcmp(av[2], "Solo") == 0)
		res += 10;
	if (ft_strcmp(av[2], "Multi") == 0)
		res += 20;
	res = res > 9 ? res : -1;
	return (res);
}

int		main(int ac, char **av)
{
	t_env	info;
	t_env	*env;

	env = &info;
	env->op = -1;
	init_choice(&env);
	if (ac != 3)
		usage();
	if ((env->op = check_params(av, env)) < 0)
		usage();
	init(env->op, &env, env->c.choice[env->op % 10]);
	fractol(&env);
	mlx_key_hook(env->win, key_hook, env);
	mlx_mouse_hook(env->win, mouse_hook, env);
	mlx_hook(env->win, 6, (1L << 6), rejuv, env);
	mlx_loop(env->mlx);
	return (0);
}
